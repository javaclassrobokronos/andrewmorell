package assignment1.Farm;

public class FarmAssignment {
	public FarmAssignment() {
		example1.BarnYard by;
		by = new example1.BarnYard();
		example1.Chicken ch;
		ch = new example1.Chicken();
		example1.Chicken ck;
		ck = new example1.Chicken();
		example1.Chicken chk;
		chk = new example1.Chicken();
		example1.Pig pi;
		pi = new example1.Pig();
		example1.Pig pg;
		pg = new example1.Pig();
		example1.Butterfly bt;
		bt = new example1.Butterfly();
		by.addChicken(ch);
		by.addChicken(ck);
		by.addChicken(chk);
		by.addPig(pg);
		by.addPig(pi);
		by.addButterfly(bt);
		ch.start();
		ck.start();
		pi.start();
		pg.start();
	}
}
