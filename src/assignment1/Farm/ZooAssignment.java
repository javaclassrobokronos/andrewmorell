package assignment1.Farm;

public class ZooAssignment {
	public ZooAssignment() {
		example1.BarnYard by1;
		by1 = new example1.BarnYard();
		example1.BarnYard by2;
		by2 = new example1.BarnYard();
		example1.BarnYard by3;
		by3 = new example1.BarnYard ();
		example1.Chicken ch1;
		ch1 = new example1.Chicken();
		example1.Chicken ch2;
		ch2 = new example1.Chicken();
		example1.Butterfly bt1;
		bt1 = new example1.Butterfly();
		example1.Butterfly bt2;
		bt2 = new example1.Butterfly();
		example1.Butterfly bt3;
		bt3 = new example1.Butterfly();
		example1.Pig pg1;
		pg1 = new example1.Pig();
		example1.Pig pg2;
		pg2 = new example1.Pig();
		by1.addChicken(ch1);
		by1.addChicken(ch2);
		by2.addButterfly(bt1);
		by2.addButterfly(bt2);
		by2.addButterfly(bt3);
		by3.addPig(pg1);
		by3.addPig(pg2);
		ch1.start();
		bt1.start();
		bt2.start();
		bt3.start();
		pg1.start();
	}
}
