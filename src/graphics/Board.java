package graphics;

import javax.swing.JButton;
import javax.swing.JFrame;
import data.Click;
import data.GameInfo;
import data.Tile;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
public class Board {
	private JFrame _jf;
	private ArrayList<ArrayList<JButton>> cells;
	private GameInfo _bc;
	public Board(GameInfo gf){
	_bc = gf;
	_jf = new JFrame("Five In A Row");
	_jf.setVisible(true);
	_jf.setLayout(new GridLayout(12,12));
	
	cells = new ArrayList<ArrayList<JButton>>();
	for(int i=0; i<12; i=i+1){
		cells.add(new ArrayList<JButton>());
		for(int j=0; j<12; j=j+1){
			ActionListener e = new Click(i, j, _bc, this);
			JButton b = new JButton ();
			b.addActionListener(e);
			cells.get(i).add(b);
		}
	}	
	
	for(int i=0; i<12; i=i+1){
		for(int j=0; j<12; j=j+1){
			_jf.add(cells.get(i).get(j));
		}
	}
	_jf.setSize(800, 800);

	}
	public JButton GetJButton(int i, int j) {
		return cells.get(i).get(j);
	}
	
	public void updating(){
	for(int i=0; i<12; i=i+1){
	for (int j=0; j<12; j=j+1){
		Tile t = _bc.GetTile(i, j);
		if(t.getString().equals("x")){
			GetJButton(i,j).setText("X");
		}
		else if (t.getString().equals("o")){
			GetJButton(i,j).setText("O");
		}
		else {
			GetJButton(i,j).setText("");
		}
	}
	
	}
	}
}
