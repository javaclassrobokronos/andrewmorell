package data;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import data.GameInfo;
import graphics.Board;
public class Click implements ActionListener{
	private int _x;
	private int _y;
	private GameInfo _cg;
	private Board _bb;
	public Click(int i, int j, GameInfo gf, Board b){
		_bb = b;
		_x = i; 
		_y = j;
		_cg = gf;
		
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		if (_cg.getplayerturn()){
			_cg.GetTile(_x,_y).setString("x");
			
		}
		else{
			_cg.GetTile(_x, _y).setString("o");
		}
		_bb.updating();
		_cg.Switch();
	}
}


