package data;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;

import graphics.Board;

public class GameInfo {
	private boolean playerturn;
	private Board _b;
	private ArrayList<ArrayList<Tile>> pie;
	public GameInfo(){
		_b = new Board(this);
		playerturn = true;
		pie = new ArrayList<ArrayList<Tile>>();
		for(int i = 0; i<12; i=i+1){
			pie.add(new ArrayList<Tile>());
			for(int j = 0; i<12; j=j+1){
				Tile t = new Tile();
				pie.get(i).add(t);
			}
		}
	}
	
	public Tile GetTile (int i, int j){
		return pie.get(i).get(j);
	}
	public boolean getplayerturn(){
		return playerturn;
		
	}
	 public void Switch(){
		 playerturn = !playerturn;
	 }
}
